
/*
* #1 Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
Екранування служить, щоб повернути спеціальному символу його звичайне рядкове значення.
*
*
* #2 Які засоби оголошення функцій ви знаєте?
* За допомогою ключового слова function.
*
* function declaration
* function expression
* arrow function expression
*

* #3 Що таке hoisting, як він працює для змінних та функцій?
* Hoisting або підняття – це можливість отримувати доступ до функцій та змінних до того,
*як вони були створені. Це механізм відноситься лише до оголошення функцій та змінних.

*/


function createNewUser(user) {
  user = {
     firstName: "",
     lastName: "",
     birthDay: "",

     getPassword() {
         return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDay[2]);
     },
     getAge() {
         let todayDate = new Date();
         new Date(todayDate.toLocaleDateString('en-GB'));

         let years = (todayDate.getFullYear() - this.birthDay[2]);
         if (todayDate.getMonth() < this.birthDay[1] ||
             todayDate.getMonth() === this.birthDay[1] && todayDate.getDate() < this.birthDay[0]) {
             years--;
             return years;
         }
     },
 }

user.firstName = prompt("Enter your name");
user.lastName = prompt("Enter your last name");
user.birthDay = prompt("Enter your date of birth 'dd.mm.yyyy'").split(".");

console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

}
createNewUser();